from gql import gql, Client
from gql.transport.aiohttp import AIOHTTPTransport



def fun(d, key):
    if key in d:
        yield d[key]
    for k in d:
        if isinstance(d[k], list):
            for i in d[k]:
                for j in fun(i, key):
                    yield j


# Select your transport with a defined url endpoint
transport = AIOHTTPTransport(url="https://api.new.tsri.ch/")

# Create a GraphQL client using the defined transport
client = Client(transport=transport, fetch_schema_from_transport=True)

# Provide a GraphQL query
query = gql(
    """
    query getArticles{
      articles(first: 6) {
        nodes {
          title
          lead
          authors{
            name
          }
          blocks {
            ... on RichTextBlock {
            richText
            }
          }
        }
      }
    }
"""
)


# Execute the query on the transport
result = client.execute(query)

printArticle = []
articles = result.get('articles')
nodes = articles.get('nodes')
title = nodes[5].get('title')
lead = nodes[5].get('lead')
printArticle.append({"title": title})
printArticle.append({"lead": lead})


#print(printArticle)

authors = nodes[5].get('authors')
author = authors[0].get('name')
texts = nodes[5].get('blocks')
text = texts[2].get('richText')

print(list(fun(text[2], 'text')))

tx = text[1].get('children')


#print(articlesText)
#print(title, "\n\n", lead, "\n\n")
#print(tx[0].get('text'))